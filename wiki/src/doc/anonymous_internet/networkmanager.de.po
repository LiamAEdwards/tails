# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-05-31 09:30+0200\n"
"PO-Revision-Date: 2022-06-11 22:39+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: Tails Translations <tails-l10n@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Connecting to a network\"]]\n"
msgid "[[!meta title=\"Connecting to a local network\"]]\n"
msgstr "[[!meta title=\"Eine Netzwerkverbindung herstellen\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Title =
#, fuzzy, no-wrap
msgid "Connecting to a local network"
msgstr "[[!meta title=\"Eine Netzwerkverbindung herstellen\"]]\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "You can connect to a network using a wired, Wi-Fi, or mobile broadband "
#| "connection:"
msgid ""
"You can connect to a local network using a wired, Wi-Fi, or mobile data "
"connection:"
msgstr ""
"Sie können sich mit einem kabelgebundenen Netzwerk, Wi-Fi- oder einem "
"mobilen Breitbandnetzwerk (Mobile Broadband) verbinden:"

#. type: Bullet: '1. '
msgid "Open the system menu in the top-right corner."
msgstr "Öffnen Sie das Systemmenü in der oberen, rechten Ecke."

#. type: Plain text
#, no-wrap
msgid "   [[!img doc/first_steps/desktop/system.png link=\"no\"]]\n"
msgstr "   [[!img doc/first_steps/desktop/system.png link=\"no\"]]\n"

#. type: Bullet: '   - '
#, fuzzy
msgid ""
"If a wired connection is detected, Tails automatically connects to the "
"network."
msgstr ""
"Wenn eine kabelgebundene Verbindung erkannt wird, verbindet sich Tails "
"automatisch mit dieser."

#. type: Bullet: '   - '
#, fuzzy
msgid ""
"To connect to a Wi-Fi network, choose <span class=\"guilabel\">Wi-Fi Not "
"Connected</span> and then <span class=\"guilabel\">Select Network</span>."
msgstr ""
"Wählen Sie <span class=\"guilabel\">Wi-Fi</span> und anschließend <span "
"class=\"guilabel\">Wählen Sie ein Netzwerk aus</span>."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<div class=\"note\">\n"
msgid "   <div class=\"bug\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p>If there is no option to connect to a Wi-Fi network, your Wi-Fi interface\n"
"   is not working in Tails. See our documentation on [[troubleshooting Wi-Fi\n"
"   not working|no-wifi]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr "   </div>\n"

#. type: Bullet: '   - '
#, fuzzy
msgid ""
"To connect to a mobile data network, choose <span class=\"guilabel\">Mobile "
"Broadband</span>."
msgstr "Wählen Sie <span class=\"guilabel\">Mobile Broadband</span> aus."

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"   <div class=\"note\">\n"
"   <p>It is currently impossible to connect to a network using:</p>\n"
msgstr "<p>Es ist momentan nicht möglich, eine Verbindung herzustellen mit:</p>\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"   <ul>\n"
"   <li>Dial-up modems.</li>\n"
"   <li>VPNs, see [[the corresponding FAQ|support/faq#vpn]].</li>\n"
"   </ul>\n"
msgstr ""
"<ul>\n"
"<li>Einwahlmodems, lesen Sie [[!tails_ticket 5913]].</li>\n"
"<li>VPNs, lesen Sie [[die zugehörigen FAQ.|support/faq#vpn]]</li>\n"
"</ul>\n"

#. type: Bullet: '1. '
msgid ""
"After you connect to a local network, the *Tor Connection* assistant appears "
"to help you [[connect to the Tor network|tor]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img tor/tor-connection.png link=\"no\"]]\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"For more information, open\n"
"<span class=\"application\">[[GNOME Help|first_steps/desktop#help]]</span>\n"
"and choose <span class=\"guilabel\">Networking, web & email</span>.\n"
msgstr ""
"Öffnen Sie für ausführlichere Informationen die\n"
"<span class=\"application\">[[GNOME Hilfe|first_steps/desktop#help]]</span>\n"
"und wählen Sie <span class=\"guilabel\">Netzwerk, Web, E-Mail und Chat</span> aus.\n"

#. type: Title =
#, no-wrap
msgid "Saving your network settings"
msgstr ""

#. type: Plain text
#, fuzzy
msgid ""
"To remember the password for Wi-Fi networks and custom network "
"configurations across different Tails sessions, turn on the [[Network "
"Connections|first_steps/persistence#network_connections]] feature of the "
"Persistent Storage."
msgstr ""
"Falls Sie Ihre angepasste Konfiguration\n"
"oder die Passwörter von verschlüsselten Drahtlosverbindungen\n"
"über mehrere Arbeitssitzungen hinweg wiederverwenden möchten, können Sie die "
"[[Funktion für\n"
"<span class=\"guilabel\">Netzwerkverbindungen</span> des beständigen "
"Speicherbereichs|first_steps/persistence/configure#network_connections]]\n"
" aktivieren.\n"

#. type: Title =
#, no-wrap
msgid "Modifying your network settings"
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid ""
#| "To modify your network settings, for example, to configure whether or not "
#| "to automatically connect to a Wi-Fi network, do the following:"
msgid ""
"To modify your network settings, for example, to configure whether or not to "
"automatically connect to a Wi-Fi network, choose **Applications&nbsp;▸ "
"System Tools&nbsp;▸ Settings&nbsp;▸ Network**."
msgstr ""
"Tun sie folgendes, um Ihre Netzwerkeinstellungen zu verändern, "
"beispielsweise um zu konfigurieren, ob automatisch eine Verbindung mit einem "
"Wi-Fi-Netzwerk hergestellt werden soll oder nicht:"

#~ msgid ""
#~ "Click on the [[!img lib/preferences-system.png alt=\"System\" "
#~ "class=\"symbolic\" link=\"no\"]] button to open the system settings."
#~ msgstr ""
#~ "Klicken Sie auf die Schaltfläche [[!img lib/preferences-system.png "
#~ "alt=\"System\" class=\"symbolic\" link=\"no\"]], um die "
#~ "Systemeinstellungen zu öffnen."

#~ msgid "Choose <span class=\"guilabel\">Network</span>."
#~ msgstr "Wählen Sie <span class=\"guilabel\">Netzwerk</span> aus."

#~ msgid "After establishing a connection to a network:"
#~ msgstr "Nachdem eine Verbindung mit einem Netzwerk hergestellt wurde:"

#, fuzzy
#~ msgid ""
#~ "If you can already access the Internet, Tor is automatically started."
#~ msgstr ""
#~ "  a. Wenn Sie bereits Internetzugriff haben, wird Tor automatisch "
#~ "gestartet.\n"

#, fuzzy
#~ msgid ""
#~ "If you need to log in to a captive portal before being granted access to "
#~ "the Internet, see our documentation on [[logging in to captive portals|"
#~ "doc/anonymous_internet/unsafe_browser]]."
#~ msgstr ""
#~ "  b. Falls Sie sich in ein Captive Portal einloggen müssen, bevor Ihnen "
#~ "Zugang\n"
#~ "     zum Internet gewährt wird, [[lesen Sie die zugehörige Dokumentation|"
#~ "unsafe_browser]].\n"

#, fuzzy
#~| msgid ""
#~| "If you are concerned about being identified as a Tails user on your "
#~| "network, read our documentation about [[network fingerprinting|doc/about/"
#~| "fingerprint]]."
#~ msgid ""
#~ "If you are concerned about being identified as a Tails user on your "
#~ "network, read our documentation about [[network fingerprinting|tor/hide]]."
#~ msgstr ""
#~ "Falls Sie Bedenken haben, als Nutzer von Tails in Ihrem Netzwerk "
#~ "identifiziert zu werden, lesen Sie unsere Dokumentation zu [[Netzwerk-"
#~ "Fingerabdrücken|doc/about/fingerprint]]."

#~ msgid ""
#~ "To connect to Tor using bridges or configure a proxy to access the "
#~ "Internet, you need to [[activate additional Tor configuration when "
#~ "starting Tails|first_steps/welcome_screen#additional]]."
#~ msgstr ""
#~ "Um sich mit Hilfe von Bridges mit Tor zu verbinden oder einen Proxy für "
#~ "den Internetzugriff zu konfigurieren, ist es nötig [[beim Start von Tails "
#~ "zusätzliche Konfigurationen für Tor zu aktivieren|first_steps/"
#~ "welcome_screen#additional]]."

#~ msgid ""
#~ "[[!inline pages=\"doc/anonymous_internet/networkmanager/no-wifi.inline\" "
#~ "raw=\"yes\" sort=\"age\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"doc/anonymous_internet/networkmanager/no-wifi.inline."
#~ "de\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "To connect to a Wi-Fi network:"
#~ msgstr "Um eine Verbindung mit einem Wi-Fi-Netzwerk herzustellen:"

#~ msgid "To connect to a mobile broadband network:"
#~ msgstr "Um sich mit einem mobilen Breitbandnetzwerk zu verbinden:"

#, fuzzy
#~ msgid "Connecting to a network\n"
#~ msgstr "[[!meta title=\"Eine Netzwerkverbindung herstellen\"]]\n"

#~ msgid ""
#~ "If you want to reuse your custom\n"
#~ "configuration or the passwords of encrypted wireless connections across "
#~ "separate\n"
#~ "working sessions, you can activate the [[<span class=\"guilabel\">Network "
#~ "connections</span> persistence\n"
#~ "feature|first_steps/persistence/configure#network_connections]].\n"
#~ msgstr ""
#~ "Falls Sie Ihre angepasste Konfiguration\n"
#~ "oder die Passwörter von verschlüsselten Drahtlosverbindungen\n"
#~ "über mehrere Arbeitssitzungen hinweg wiederverwenden möchten, können Sie "
#~ "die [[Funktion für\n"
#~ "<span class=\"guilabel\">Netzwerkverbindungen</span> des beständigen "
#~ "Speicherbereichs|first_steps/persistence/configure#network_connections]]\n"
#~ " aktivieren.\n"
